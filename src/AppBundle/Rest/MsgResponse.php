<?php

namespace AppBundle\Rest;

use JMS\Serializer\Annotation as Serializer;

/**
 * @Serializer\XmlRoot(name="response")
 * @Serializer\ExclusionPolicy("all")
 *
 * Class Response
 */
class MsgResponse
{
    /**
     * @Serializer\Expose()
     *
     * @var string
     */
    private $msg;

    /**
     * @Serializer\Expose()
     * @Serializer\SerializedName("result")
     *
     * @var array
     */
    private $data;

    /**
     * Response constructor.
     * @param string $msg
     * @param array|object $data
     */
    public function __construct($msg, $data)
    {
        $this->msg = $msg;
        $this->data = $data;
    }

    /**
     * @return string
     */
    public function getMsg() {
        return $this->msg;
    }

    /**
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }
}