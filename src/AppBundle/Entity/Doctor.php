<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Table(name="lumeon_doctor")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\DoctorRepository")
 * @JMS\ExclusionPolicy("all")
 * @JMS\XmlRoot("doctor")
 */
class Doctor
{
    /**
     * @JMS\Expose()
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * 
     * @var int
     */
    private $id;

    /**
     * @JMS\Expose()
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     * 
     * @var string
     */
    private $name;

    /**
     * @JMS\Expose()
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Patient", inversedBy="doctors")
     * @ORM\JoinTable(name="lumeon_doctors_patients")
     * 
     * @var Patient[]|ArrayCollection
     */
    private $patients;

    public function __construct() {
        $this->patients = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Doctor
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add a patient
     *
     * @param Patient $patient
     *
     * @return Doctor
     */
    public function addPatient(Patient $patient)
    {
        $this->patients[] = $patient;

        return $this;
    }

    /**
     * Get patients
     *
     * @return ArrayCollection|Patient[]
     */
    public function getPatients()
    {
        return $this->patients;
    }
}