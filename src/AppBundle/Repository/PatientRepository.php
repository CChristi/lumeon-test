<?php

namespace AppBundle\Repository;


use AppBundle\Entity\Patient;
use Doctrine\ORM\EntityRepository;

class PatientRepository extends EntityRepository implements RepositoryInterface
{
    /** @return Patient */
    public function selectById($id)
    {
        return $this->findOneById($id);
    }
}