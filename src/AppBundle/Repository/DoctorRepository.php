<?php
/**
 * Created by PhpStorm.
 * User: ccristi
 * Date: 3/23/17
 * Time: 6:24 PM
 */

namespace AppBundle\Repository;


use Doctrine\ORM\EntityRepository;

class DoctorRepository extends EntityRepository implements RepositoryInterface
{
    /**
     * @param string $id
     */
    public function selectById($id)
    {
        return $this->findOneById($id);
    }
}