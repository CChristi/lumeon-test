<?php

namespace AppBundle\Repository;


use AppBundle\Entity\Hospital;
use Doctrine\ORM\EntityRepository;

class HospitalRepository extends EntityRepository implements RepositoryInterface
{
    /** @return Hospital */
    public function selectById($id)
    {
        return $this->findOneById($id);
    }
}