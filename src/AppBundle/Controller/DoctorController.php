<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Patient;
use AppBundle\Entity\Doctor;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\Controller\FOSRestController;
use AppBundle\Rest\MsgResponse;
use FOS\RestBundle\Request\ParamFetcher;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class DoctorController extends FOSRestController
{
    /**
     * @RequestParam(name="doctor_id", requirements="\d+", description="Doctor ID")
     * @RequestParam(name="patient_id", requirements="\d+", description="Patient ID")
     *
     * @param ParamFetcher $paramFetcher
     * @return MsgResponse
     */
    function postDoctorPatientAction(ParamFetcher $paramFetcher) {
        $em = $this->getDoctrine()->getEntityManager();

        $doctorId = $paramFetcher->get('doctor_id');
        $patientId = $paramFetcher->get('patient_id');

        /** @var Doctor $doctor */
        $doctor = $em->getRepository(Doctor::class)->findOneById($doctorId);

        if (!$doctor) {
            throw new NotFoundHttpException('DoctorNotFound');
        }

        /** @var Patient $patient */
        $patient = $em->getReference(Patient::class, $patientId);

        $doctor->addPatient($patient);

        try {
            $em->flush();
        } catch (UniqueConstraintViolationException $e) {
            throw new ConflictHttpException('PatientAlreadyLinked');
        } catch (ForeignKeyConstraintViolationException $e) {
            throw new NotFoundHttpException('PatientNotFound');
        }

        return new MsgResponse('PatientLinked', $doctor);
    }
}