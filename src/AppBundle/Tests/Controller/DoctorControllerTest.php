<?php

namespace AppBundle\Tests\Controller;

use AppBundle\DataFixtures\ORM\DoctorData;
use AppBundle\DataFixtures\ORM\PatientData;
use AppBundle\Rest\MsgResponse;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Loader;
use Liip\FunctionalTestBundle\Test\WebTestCase;

class DoctorControllerTest extends WebTestCase
{
    /**
     * Test link doctor patient functionality
     */
    public function testPostDoctorPatient()
    {
        $client = static::createClient();
        $serializer = $client->getContainer()->get('jms_serializer');

        $fixturesRepository = $this->loadFixtures(array(
            DoctorData::class,
            PatientData::class
        ))->getReferenceRepository();

        $doctor = $fixturesRepository->getReference('doctor');
        $patient = $fixturesRepository->getReference('patient');

        $client->request(
            'POST',
            '/lumeon/doctors/patients',
            [
                'doctor_id' => $doctor->getId(),
                'patient_id' => $patient->getId(),
            ],
            [],
            ['HTTP_ACCEPT' => 'application/json']
        );

        $response = json_decode($client->getResponse()->getContent(), true);

        $this->assertSame(
            $response,
            $serializer->toArray(new MsgResponse('PatientLinked', $doctor))
        );
    }
}