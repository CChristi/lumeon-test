<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Patient;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;

class PatientData extends AbstractFixture
{
    private $data = [
      ['Joe', Patient::GENDER_MALE, '1990-01-01'],
      ['Rachel', Patient::GENDER_FEMALE, '1991-10-01'],
    ];

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        foreach ($this->data as list($name, $gender, $rawDate)) {
            $patient = new Patient();

            $patient->setName($name);
            $patient->setGender($gender);
            $patient->setDob(new \DateTime($rawDate));
            
            $manager->persist($patient);
        }

        $masterPatient = new Patient();

        $masterPatient->setName('master');
        $masterPatient->setGender(Patient::GENDER_MALE);
        $masterPatient->setDob(new \DateTime());

        $manager->persist($masterPatient);
        $manager->flush();

        $this->addReference('patient', $masterPatient);
    }
}