<?php
/**
 * Created by PhpStorm.
 * User: ccristi
 * Date: 3/24/17
 * Time: 8:33 AM
 */

namespace AppBundle\DataFixtures\ORM;


use AppBundle\Entity\Doctor;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;

class DoctorData extends AbstractFixture
{
    private $data = [
        ['Poe'],
        ['Phil'],
        ['James'],
    ];

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        foreach ($this->data as list($name)) {
            $doctor = new Doctor();

            $doctor->setName($name);
            
            $manager->persist($doctor);
        }

        $masterDoctor = new Doctor();
        $masterDoctor->setName('master');

        $manager->persist($masterDoctor);
        $manager->flush();

        $this->addReference('doctor', $masterDoctor);
    }
}